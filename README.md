# Mutt-Examples

# Neovim examples

`~/.config/neomutt/`

And private files in:

`~/.neomutt`

So that I can easily sort out which files need to be encrypted using 
[yadm encryption](https://yadm.io/docs/encryption)

# Requirements

neomutt and lmdb

debian-like:

```
sudo apt install neomut liblmdb-dev
```

Arch linux:

```
sudo pacman -Sy neomutt lmdb
```

# Mutt Examples

There is a legacy `mutt` branch.

mutt examples may be broken!  please make an issue and let me know!

For this mutt setup I recommend placing public files in:

`~/.config/mutt/`

And private files in:

`~/.mutt`
